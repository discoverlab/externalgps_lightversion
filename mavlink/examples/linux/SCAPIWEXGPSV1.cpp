#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <netinet/in.h>
#include <resolv.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <iostream>


#include "../../include/autoquad/mavlink.h"
//#include <mavlink_msg_aq_robotmsg.h>
#define EXTERNALGPS_IPADDRESS "192.168.1.145"
#pragma warning( disable : 4996 )
#define MAX_NAMELENGTH 256
#define MULTICAST_ADDRESS "239.255.42.99"     		// IANA, local network
#define PORT_DATA 1511                			// Default multicast group
#define GUMSTIX_IPADDRESS "192.168.1.42"

class ExternalGPS {
public:
    // Constructor / Destructor
    ExternalGPS(void);
    virtual ~ExternalGPS();


    void set_values(int, char*, char*);


    double Lat_mav,Lon_mav,Alt_mav;
    float Yaw_mav;

    int init(void);

private:
    int quadID;
    char szMyIPAddress[128];
    char szServerIPAddress[128];
    float x_n,y_n,z_n,yaw_n,x_ref,y_ref,z_ref,yaw_ref,pitch,roll,gaz,yaw;
    float q_x,q_y,q_z,q_w;
    int DataSocket;


    pthread_t recving;
    virtual void loop(void);
    static void *run(void *args) {
        reinterpret_cast<ExternalGPS*>(args)->loop();
        return NULL;
    }

    char  szData[20000];
	int addr_len;
	struct sockaddr_in TheirAddress;

    // Analyse the data
    void Unpack(char*,int);
    void RadiansToDegrees(float *value);
    void GetEulers(float qx, float qy, float qz, float qw, float *angle1,float *angle2, float *angle3);
};




typedef struct {
    double x,y,z;
    double heading;
    int ID;
    unsigned int cmd;
} robot_state_t;

#define MAX_ROBOT_NO	4
#define MAX_ROBOT_STATE_BUFF_SIZE 	100

typedef struct {
	int thread_no;
	robot_state_t *rbstate_p;
	mavlink_message_t *UAVmsg_p;
	int *hsock;

	robot_state_t rbstate_buff[MAX_ROBOT_NO][MAX_ROBOT_STATE_BUFF_SIZE];
} thread_data_t;

void* SocketHandler(void*);
void* ServerThread(void* arg);
int SendToServer(mavlink_message_t *UAVmsg,int hsock);
int SockInit(char* host_name,int host_port);
void * ClientThread(void * argument);

#define BUFFER_LENGTH 2041 // minimum buffer size that can be used with qnx (I don't know why)

uint64_t microsSinceEpoch();

#define UAV_IP 		"127.0.0.1"//"192.168.1.137"
#define UAV_PORT 	6667

 uint16_t buff_length;
 uint8_t * buff_msg;

int main(int argv, char** argc){

ExternalGPS externalgps;

 externalgps.set_values(2,GUMSTIX_IPADDRESS,EXTERNALGPS_IPADDRESS);
    if(externalgps.init() != 1){
        printf("ExternalGPS error: fail to initialize\n");
        exit(-1);
    }

	//int testtest = MAVLINK_VERSION;

	char target_ip[100];
	robot_state_t robot_state;
	robot_state_t rec_rbstate; // the recived robot state
	robot_state_t rec_rbstate_buff[MAX_ROBOT_NO][MAX_ROBOT_STATE_BUFF_SIZE];

	/* MAV msg variable */

	 mavlink_message_t msgp;
	 mavlink_aq_robotmsg_t *robot_msg;
	 uint16_t UAV_buff_len;
	 uint8_t UAV_buff[BUFFER_LENGTH];

    thread_data_t srvth_data,clnth_data;

    clnth_data.rbstate_p=&robot_state;
    srvth_data.rbstate_p=&rec_rbstate;

    clnth_data.UAVmsg_p=&msgp;

    //srvth_data.rbstate_buff =rec_rbstate_buff;

    // Change the target ip if parameter was given
   /* strcpy(target_ip, "127.0.0.1");
    if (argc == 2)
        {
    		strcpy(target_ip, argv[1]);
        }*/

    pthread_t t1 ;
   // pthread_create(&t1,NULL,ServerThread,(void *) &srvth_data);

    pthread_t t_client;
    pthread_create( &t_client, 0, ClientThread,(void *) &clnth_data);

    long counter=0;
    long ctr=0;




    while(true){
        if (counter==100000){

        	/*Send Heartbeat */
        	//printf("call robot msg pack\n");
        	mavlink_msg_aq_robotmsg_pack2(2, 2,  &msgp, 2, 1, 1, 1, 1, 1, 1);

             //printf("\packed packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d\n", msgp.sysid, msgp.compid, msgp.len, msgp.msgid);

               counter=0;

        }
        counter++;
    }
    pthread_join(t_client,NULL) ;
//    close(hsock1);
//    close(hsock2);
    return 0;

}

#define MAV_LINK_ROBOTMSG_LEN 32
void* SocketHandler(void* lp){
	thread_data_t *thread_data;
	thread_data = (thread_data_t *) lp;  /* type cast to a pointer to thdata */
	int *csock = thread_data->hsock;

    	robot_state_t robot_state;

    	mavlink_message_t msg;
        mavlink_status_t status;
    	uint8_t buffer[BUFFER_LENGTH],temp;
	int msg_recv_length;
        int bytecount,i;

        uint16_t  len = MAV_LINK_ROBOTMSG_LEN;
        //char * buff= (char*) malloc(len);
        char server_reply[9]="Received";

        memset(buffer, 0, BUFFER_LENGTH);
        while((bytecount = recv(*csock, buffer, len,0))>0){



			for (i = 0; i < len; ++i)
			{
				temp = buffer[i];
				printf("%02x ", (unsigned char)temp);
				if (mavlink_parse_char(MAVLINK_COMM_0, buffer[i], &msg, &status))
				{
					// Packet received
					printf("\nReceived packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d\n", msg.sysid, msg.compid, msg.len, msg.msgid);
				}
			}



       // *(thread_data->UAVmsg_p)=msg;

        printf("SocketHandler: Received bytes %d\n", bytecount);
        write(*csock , server_reply , strlen(server_reply));
        }
        if(bytecount  == 0){
            fprintf(stderr, "Client disconnected %d\n", errno);
        }
        else if(bytecount == -1){
            fprintf(stderr, "SocketHandler: Error receiving data %d\n", errno);
        }
        FINISH:
//            free(buff);
            free(csock);
            return 0;
}

void* ServerThread(void * arg){

	thread_data_t *thread_data;
	thread_data = (thread_data_t *) arg;  /* type cast to a pointer to thdata */
	int host_port= 6667;

    struct sockaddr_in my_addr;

    int hsock;
    int * p_int;


    socklen_t addr_size = 0;
    int* csock;
    sockaddr_in sadr;
    pthread_t thread_id=0;


    hsock = socket(AF_INET, SOCK_STREAM, 0);
    if(hsock == -1){
        printf("ServerThread: Error initializing socket %d\n", errno);
        goto FINISH;
    }

    p_int = (int*)malloc(sizeof(int));
    *p_int = 1;

    if( (setsockopt(hsock, SOL_SOCKET, SO_REUSEADDR, (char*)p_int, sizeof(int)) == -1 )||
       (setsockopt(hsock, SOL_SOCKET, SO_KEEPALIVE, (char*)p_int, sizeof(int)) == -1 ) ){
        printf("ServerThread: Error setting options %d\n", errno);
        free(p_int);
        goto FINISH;
    }
    free(p_int);

    my_addr.sin_family = AF_INET ;
    my_addr.sin_port = htons(host_port);

    memset(&(my_addr.sin_zero), 0, 8);
    my_addr.sin_addr.s_addr = INADDR_ANY ;

    if( bind( hsock, (sockaddr*)&my_addr, sizeof(my_addr)) == -1 ){
        fprintf(stderr,"ServerThread: Error binding to socket, make sure nothing else is listening on this port %d\n",errno);
        goto FINISH;
    }
    if(listen( hsock, 10) == -1 ){
        fprintf(stderr, "ServerThread: Error listening %d\n",errno);
        goto FINISH;
    }

    //Now lets do the server stuff

    addr_size = sizeof(sockaddr_in);

    while(true){
        printf("ServerThread: waiting for a connection\n");
        csock = (int*)malloc(sizeof(int));
        if((*csock = accept( hsock, (sockaddr*)&sadr, &addr_size))!= -1){
        	thread_data->hsock=csock;

            printf("---------------------\nReceived connection from %s\n",inet_ntoa(sadr.sin_addr));
            //pthread_create(&thread_id,0,&SocketHandler, (void*)csock );
            pthread_create(&thread_id,0,&SocketHandler, (void*)thread_data );
            pthread_detach(thread_id);
        }
        else{
            fprintf(stderr, "ServerThread: Error accepting %d\n", errno);
        }
    }

FINISH:
    close(hsock);
    return 0;
}

int SendToServer( mavlink_message_t *msg_p,int hsock){

    int bytecount;
    char server_reply[2000];
    int len;
    uint8_t msg_send_buf[BUFFER_LENGTH];
   int 	msg_length=0;
   msg_length = mavlink_msg_to_send_buffer(msg_send_buf, msg_p);

   mavlink_message_t msg;
   mavlink_status_t status;
   for(int i=0;i<msg_length;i++)
   	if(mavlink_parse_char(MAVLINK_COMM_0, msg_send_buf[i], &msg, &status))
		printf("success\n");

    //printf("msg_length %d\n", msg_length);

    if((bytecount=send(hsock,  msg_send_buf, msg_length,0))== -1){
        //fprintf(stderr, "SendToServer: Error sending data %d\n", errno);
       // free(buffer);
        return -1;
    }
   // printf("SendToServer: rs.x=%4.4f \n", rs->x);
    printf("SendToServer: Sent bytes %d\n", bytecount);
/*
    if( recv(hsock , server_reply , len , 0) < 0){
                std::cout<<"recv failed"<<std::endl;
               // free(buffer);
                return -1;
                }
    else
                 std::cout<<"Robot 2:"<<server_reply<<std::endl;
  */
   // free(buffer);
    return 0;

}

int SockInit(char* host_name,int host_port){
    struct sockaddr_in my_addr;

    int hsock;
    int * p_int;
    int err;

    hsock = socket(AF_INET, SOCK_STREAM, 0);
    if(hsock == -1){
        printf("SockInit: Error initializing socket %d\n",errno);
        return -1;
        //goto FINISH;
    }

    p_int = (int*)malloc(sizeof(int));
    *p_int = 1;

    if( (setsockopt(hsock, SOL_SOCKET, SO_REUSEADDR, (char*)p_int, sizeof(int)) == -1 )||
       (setsockopt(hsock, SOL_SOCKET, SO_KEEPALIVE, (char*)p_int, sizeof(int)) == -1 ) ){
        printf("SockInit: Error setting options %d\n",errno);
        free(p_int);
        return -1;
        //goto FINISH;
    }
    free(p_int);

    my_addr.sin_family = AF_INET ;
    my_addr.sin_port = htons(host_port);

    memset(&(my_addr.sin_zero), 0, 8);
    my_addr.sin_addr.s_addr = inet_addr(host_name);

    if( connect( hsock, (struct sockaddr*)&my_addr, sizeof(my_addr)) == -1 ){
        if((err = errno) != EINPROGRESS){
            fprintf(stderr, "SockInit: Error connecting socket %d\n", errno);
            return -1;
            //goto FINISH;
        }
    }
    return hsock;
//FINISH:
//    return hsock;
}

void * ClientThread(void * argument){
    long long counter=0;
    int ctr=0;
   // int hsock2=SockInit("192.168.1.3",6667);
   int UAVhsock=SockInit(UAV_IP,UAV_PORT);
    mavlink_message_t msg;

    thread_data_t *thread_data;
    thread_data = (thread_data_t *) argument;  /* type cast to a pointer to thdata */

    while(true){
        if (counter==1000000){
            //std::cout<<++ctr<<std::endl;

            SendToServer(thread_data->UAVmsg_p,UAVhsock);
           // SendToServer(thread_data->rbstate_p,hsock1);
           // counter=0;
        }
        counter++;
    }

 close(UAVhsock);
}



ExternalGPS::ExternalGPS(void)
{

}

ExternalGPS::~ExternalGPS()
{

}

void ExternalGPS::set_values(int id, char *myip, char *serverip)
{
    quadID = id;
//    memcpy(szMyIPAddress,myip,sizeof(&myip));
 //   memcpy(szServerIPAddress,serverip,sizeof(&serverip));

}


// Success:1
int ExternalGPS::init(void)
{
    int retval;
     strcpy(szMyIPAddress, "192.168.1.42");
	//szMyIPAddress[128] = "192.168.1.137";		//Ardrone IP
	strcpy(szServerIPAddress, "192.168.1.145");
	//szServerIPAddress[128] = "192.168.1.145";		//Localization broadcasting IP
	in_addr MyAddress, MultiCastAddress;
	int optval = 0x100000;
	int optval_size = 4;

	MyAddress.s_addr = inet_addr(szMyIPAddress);
	MultiCastAddress.s_addr = inet_addr(MULTICAST_ADDRESS);
	printf("ExternalGPS > Client: %s\n", szMyIPAddress);
	printf("ExternalGPS > Server: %s\n", szServerIPAddress);
	printf("ExternalGPS > Multicast Group: %s\n", MULTICAST_ADDRESS);

	// create a "Data" socket
	DataSocket = socket(AF_INET, SOCK_DGRAM, 0);

	// allow multiple clients on same machine to use address/port
	int value = 1;
	retval = setsockopt(DataSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&value, sizeof(value));
	if (retval < 0 ){
		close(DataSocket);
		return 0;
	}

	struct sockaddr_in MySocketAddr;
	memset(&MySocketAddr, 0, sizeof(MySocketAddr));
	MySocketAddr.sin_family = AF_INET;
	MySocketAddr.sin_port = htons(PORT_DATA);
	MySocketAddr.sin_addr.s_addr = INADDR_ANY;

	if (bind(DataSocket, (struct sockaddr *)&MySocketAddr, sizeof(sockaddr)) < 0){
		printf("ExternalGPS > [PacketClient] bind failed\n");
		close(DataSocket);
		return 0;
	}

	// join multicast group
	struct ip_mreq Mreq;
	Mreq.imr_multiaddr = MultiCastAddress;
	Mreq.imr_interface = MyAddress;
	retval = setsockopt(DataSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&Mreq, sizeof(Mreq));

	if (retval <0){
		printf("ExternalGPS > [PacketClient] join failed. \n");
		close(DataSocket);
		return 0;
	}

	// create a 1MB buffer
	setsockopt(DataSocket, SOL_SOCKET, SO_RCVBUF, (char *)&optval, 4);
	getsockopt(DataSocket, SOL_SOCKET, SO_RCVBUF, (char *)&optval, (socklen_t*)&optval_size);
	if (optval != 0x100000){
		printf("ExternalGPS > [PacketClient] ReceiveBuffer size = %d. \n", optval);
	}

	addr_len = sizeof(struct sockaddr);

    pthread_create(&recving,NULL,run,this);

	return 1;
}


void ExternalGPS::loop(void)
{
    int init_flag = 1;
    uint8_t buf[2014];
	mavlink_message_t msg;
	uint16_t len;
    while(1){
        recvfrom(DataSocket, szData, sizeof(szData), 0, (sockaddr *)&TheirAddress, (socklen_t *)&addr_len);
        Unpack(szData,1);				//set x,y,z,yaw in Unpack()






    }
}

void ExternalGPS::Unpack(char* pData,int quadID)
{
    char *ptr = pData;

    //printf("Begin Packet\n-------\n");

    // message ID
    int MessageID = 0;
    memcpy(&MessageID, ptr, 2); ptr += 2;
    //printf("Message ID : %d\n", MessageID);

    // size
    int nBytes = 0;
    memcpy(&nBytes, ptr, 2); ptr += 2;
    //printf("Byte count : %d\n", nBytes);

    if(MessageID == 7)      // FRAME OF MOCAP DATA packet
    {
        // frame number
        int frameNumber = 0; memcpy(&frameNumber, ptr, 4); ptr += 4;
        //printf("Frame # : %d\n", frameNumber);

	    // number of data sets (markersets, rigidbodies, etc)
        int nMarkerSets = 0; memcpy(&nMarkerSets, ptr, 4); ptr += 4;
        //printf("Marker Set Count : %d\n", nMarkerSets);

        for (int i=0; i < nMarkerSets; i++)
        {
            // Markerset name
            char szName[256];
            strcpy(szName, ptr);
            int nDataBytes = (int) strlen(szName) + 1;
            ptr += nDataBytes;
           // printf("Model Name: %s\n", szName);

        	// marker data
            int nMarkers = 0; memcpy(&nMarkers, ptr, 4); ptr += 4;
            //printf("Marker Count : %d\n", nMarkers);

            for(int j=0; j < nMarkers; j++)
            {
                float x = 0; memcpy(&x, ptr, 4); ptr += 4;
                float y = 0; memcpy(&y, ptr, 4); ptr += 4;
                float z = 0; memcpy(&z, ptr, 4); ptr += 4;
                //printf("\tMarker %d : [x=%3.2f,y=%3.2f,z=%3.2f]\n",j,x,y,z);
            }
        }

	    // unidentified markers
        int nOtherMarkers = 0; memcpy(&nOtherMarkers, ptr, 4); ptr += 4;
        //printf("Unidentified Marker Count : %d\n", nOtherMarkers);
        for(int j=0; j < nOtherMarkers; j++)
        {
            float x = 0.0f; memcpy(&x, ptr, 4); ptr += 4;
            float y = 0.0f; memcpy(&y, ptr, 4); ptr += 4;
            float z = 0.0f; memcpy(&z, ptr, 4); ptr += 4;
            //printf("\tMarker %d : pos = [%3.2f,%3.2f,%3.2f]\n",j,x,y,z);
        }

        // rigid bodies
        int nRigidBodies = 0;
        memcpy(&nRigidBodies, ptr, 4); ptr += 4;
        //printf("Rigid Body Count : %d\n", nRigidBodies);
        for (int j=0; j < nRigidBodies; j++)
        {
            // rigid body pos/ori
            int ID = 0; memcpy(&ID, ptr, 4); ptr += 4;
            float x = 0.0f; memcpy(&x, ptr, 4); ptr += 4;
            float y = 0.0f; memcpy(&y, ptr, 4); ptr += 4;
            float z = 0.0f; memcpy(&z, ptr, 4); ptr += 4;
            float qx = 0; memcpy(&qx, ptr, 4); ptr += 4;
            float qy = 0; memcpy(&qy, ptr, 4); ptr += 4;
            float qz = 0; memcpy(&qz, ptr, 4); ptr += 4;
            float qw = 0; memcpy(&qw, ptr, 4); ptr += 4;

	//printf("ID : %d\n", ID);
           // printf("pos: [%3.2f,%3.2f,%3.2f]\n",-z*1000,x*1000,-y*1000);
           // printf("ori: [%3.2f,%3.2f,%3.2f,%3.2f]\n", qx,qy,qz,qw);

if (ID==2){

float yaw_l, pitch_l, roll_l;
//GetEulers(qx,qy,qz,qw,&pitch_l,&roll_l,&yaw_l);

//pitch_c=roll_l;
//roll_c=-pitch_l;

/*x_n=-z*1000;
y_n=x*1000;
z_n=-y*1000;
yaw_n=-yaw_l;*/
x_n=x;
y_n=y;
z_n=z;
yaw_n=yaw;
printf("pos:[%3.2f,%3.2f,%3.2f,%3.2f]\n",x_n,y_n,z_n,yaw_n);
//Lat_mav = x/RADIUS_EARTH_METER*180.0f/3.14159265f;
//Lon_mav = -z/RADIUS_EARTH_METER*180.0f/3.14159265f;
//Alt_mav = y;
//Yaw_mav = -yaw_l;

}

            // associated marker positions
            int nRigidMarkers = 0;  memcpy(&nRigidMarkers, ptr, 4); ptr += 4;
            //printf("Marker Count: %d\n", nRigidMarkers);
            int nBytes = nRigidMarkers*3*sizeof(float);
            float* markerData = (float*)malloc(nBytes);
            memcpy(markerData, ptr, nBytes);
            ptr += nBytes;


            // associated marker IDs
            nBytes = nRigidMarkers*sizeof(int);
            int* markerIDs = (int*)malloc(nBytes);
            memcpy(markerIDs, ptr, nBytes);
            ptr += nBytes;

            // associated marker sizes
            nBytes = nRigidMarkers*sizeof(float);
            float* markerSizes = (float*)malloc(nBytes);
            memcpy(markerSizes, ptr, nBytes);
            ptr += nBytes;

            for(int k=0; k < nRigidMarkers; k++)
            {
                //printf("\tMarker %d: id=%d\tsize=%3.1f\tpos=[%3.2f,%3.2f,%3.2f]\n", k, markerIDs[k], 			markerSizes[k], markerData[k*3], markerData[k*3+1],markerData[k*3+2]);
            }

            if(markerIDs)
                free(markerIDs);
            if(markerSizes)
                free(markerSizes);

            if(markerData)
                free(markerData);

            // Mean marker error
            float fError = 0.0f; memcpy(&fError, ptr, 4); ptr += 4;
            //printf("Mean marker error: %3.2f\n", fError);

	} // next rigid body


        // skeletons
        int nSkeletons = 0;
        memcpy(&nSkeletons, ptr, 4); ptr += 4;
        //printf("Skeleton Count : %d\n", nSkeletons);
        for (int j=0; j < nSkeletons; j++)
        {
            // skeleton id
            int skeletonID = 0;
            memcpy(&skeletonID, ptr, 4); ptr += 4;
            // # of rigid bodies (bones) in skeleton
            int nRigidBodies = 0;
            memcpy(&nRigidBodies, ptr, 4); ptr += 4;
            //printf("Rigid Body Count : %d\n", nRigidBodies);
            for (int j=0; j < nRigidBodies; j++)
            {
                    // rigid body pos/ori
                int ID = 0; memcpy(&ID, ptr, 4); ptr += 4;
                float x = 0.0f; memcpy(&x, ptr, 4); ptr += 4;
                float y = 0.0f; memcpy(&y, ptr, 4); ptr += 4;
                float z = 0.0f; memcpy(&z, ptr, 4); ptr += 4;
                float qx = 0; memcpy(&qx, ptr, 4); ptr += 4;
                float qy = 0; memcpy(&qy, ptr, 4); ptr += 4;
                float qz = 0; memcpy(&qz, ptr, 4); ptr += 4;
                float qw = 0; memcpy(&qw, ptr, 4); ptr += 4;
                //printf("ID : %d\n", ID);
               // printf("pos: [%3.2f,%3.2f,%3.2f]\n", x,y,z);
                //printf("ori: [%3.2f,%3.2f,%3.2f,%3.2f]\n", qx,qy,qz,qw);
                 // associated marker positions
                int nRigidMarkers = 0;  memcpy(&nRigidMarkers, ptr, 4); ptr += 4;
                //printf("Marker Count: %d\n", nRigidMarkers);
                int nBytes = nRigidMarkers*3*sizeof(float);
                float* markerData = (float*)malloc(nBytes);
                memcpy(markerData, ptr, nBytes);
                ptr += nBytes;

                // associated marker IDs
                nBytes = nRigidMarkers*sizeof(int);
                int* markerIDs = (int*)malloc(nBytes);
                memcpy(markerIDs, ptr, nBytes);
                ptr += nBytes;
                // associated marker sizes
                nBytes = nRigidMarkers*sizeof(float);
                float* markerSizes = (float*)malloc(nBytes);
                memcpy(markerSizes, ptr, nBytes);
                ptr += nBytes;
                for(int k=0; k < nRigidMarkers; k++)
                {
                    //printf("\tMarker %d: id=%d\tsize=%3.1f\tpos=[%3.2f,%3.2f,%3.2f]\n", k, markerIDs[k], markerSizes[k], markerData[k*3], markerData[k*3+1],markerData[k*3+2]);
                }

                // Mean marker error
                float fError = 0.0f; memcpy(&fError, ptr, 4); ptr += 4;
                //printf("Mean marker error: %3.2f\n", fError);

                // release resources
                if(markerIDs)
                    free(markerIDs);
                if(markerSizes)
                    free(markerSizes);
                if(markerData)
                    free(markerData);

            } // next rigid body

        } // next skeleton

        // latency
        float latency = 0.0f; memcpy(&latency, ptr, 4);	ptr += 4;
        //printf("latency : %3.3f\n", latency);

	// end of data tag
        int eod = 0; memcpy(&eod, ptr, 4); ptr += 4;
        //printf("End Packet\n-------------\n");

    }
    else if(MessageID == 5) // Data Descriptions
    {
        // number of datasets
        int nDatasets = 0; memcpy(&nDatasets, ptr, 4); ptr += 4;
        //printf("Dataset Count : %d\n", nDatasets);

        for(int i=0; i < nDatasets; i++)
        {
            //printf("Dataset %d\n", i);

            int type = 0; memcpy(&type, ptr, 4); ptr += 4;
            //printf("Type : %d\n", i, type);

            if(type == 0)   // markerset
            {
                // name
                char szName[256];
                strcpy(szName, ptr);
                int nDataBytes = (int) strlen(szName) + 1;
                ptr += nDataBytes;
                //printf("Markerset Name: %s\n", szName);

        	    // marker data
                int nMarkers = 0; memcpy(&nMarkers, ptr, 4); ptr += 4;
                //printf("Marker Count : %d\n", nMarkers);

                for(int j=0; j < nMarkers; j++)
                {
                    char szName[256];
                    strcpy(szName, ptr);
                    int nDataBytes = (int) strlen(szName) + 1;
                    ptr += nDataBytes;
                    //printf("Marker Name: %s\n", szName);
                }
            }
            else if(type ==1)   // rigid body
            {
                // name
                char szName[MAX_NAMELENGTH];
                strcpy(szName, ptr);
                ptr += strlen(ptr) + 1;
                //printf("Name: %s\n", szName);


                int ID = 0; memcpy(&ID, ptr, 4); ptr +=4;
                //printf("ID : %d\n", ID);

                int parentID = 0; memcpy(&parentID, ptr, 4); ptr +=4;
                //printf("Parent ID : %d\n", parentID);

                float xoffset = 0; memcpy(&xoffset, ptr, 4); ptr +=4;
                //printf("X Offset : %3.2f\n", xoffset);

                float yoffset = 0; memcpy(&yoffset, ptr, 4); ptr +=4;
                //printf("Y Offset : %3.2f\n", yoffset);

                float zoffset = 0; memcpy(&zoffset, ptr, 4); ptr +=4;
                //printf("Z Offset : %3.2f\n", zoffset);

            }
            else if(type ==2)   // skeleton
            {
                char szName[MAX_NAMELENGTH];
                strcpy(szName, ptr);
                ptr += strlen(ptr) + 1;
                //printf("Name: %s\n", szName);

                int ID = 0; memcpy(&ID, ptr, 4); ptr +=4;
                //printf("ID : %d\n", ID);

                int nRigidBodies = 0; memcpy(&nRigidBodies, ptr, 4); ptr +=4;
                //printf("RigidBody (Bone) Count : %d\n", nRigidBodies);

                for(int i=0; i< nRigidBodies; i++)
                {
                    // RB name
                    char szName[MAX_NAMELENGTH];
                    strcpy(szName, ptr);
                    ptr += strlen(ptr) + 1;
                    //printf("Rigid Body Name: %s\n", szName);

                    int ID = 0; memcpy(&ID, ptr, 4); ptr +=4;
                    //printf("RigidBody ID : %d\n", ID);

                    int parentID = 0; memcpy(&parentID, ptr, 4); ptr +=4;
                    //printf("Parent ID : %d\n", parentID);

                    float xoffset = 0; memcpy(&xoffset, ptr, 4); ptr +=4;
                    //printf("X Offset : %3.2f\n", xoffset);

                    float yoffset = 0; memcpy(&yoffset, ptr, 4); ptr +=4;
                    //printf("Y Offset : %3.2f\n", yoffset);

                    float zoffset = 0; memcpy(&zoffset, ptr, 4); ptr +=4;
                    //printf("Z Offset : %3.2f\n", zoffset);
                }
            }

        }   // next dataset

      // printf("End Packet\n-------------\n");

    }
    else
    {
        //printf("Unrecognized Packet Type.\n");
    }

}

void ExternalGPS::RadiansToDegrees(float *value)
{
    *value = (*value)*(180.0f/3.14159265f);
}
/*
void ExternalGPS::GetEulers(float qx, float qy, float qz, float qw, float *angle1,float *angle2, float *angle3)
{
    float &heading = *angle1;
	float &attitude = *angle2;
	float &bank = *angle3;
	double test = qw*qx + qy*qz;
	if (test > 0.499) { 					// singularity at north pole
		heading = (float) 2.0f * atan2(qy,qw);
		attitude = 3.14159265f/2.0f;
		bank = 0;
		RadiansToDegrees(&heading);
		RadiansToDegrees(&attitude);
		RadiansToDegrees(&bank);
		return;
	}
	if (test < -0.499) {						// singularity at south pole
		heading = (float) -2.0f * atan2(qy,qw);
		attitude = - 3.14159265f/2.0f;
		bank = 0;
		RadiansToDegrees(&heading);
		RadiansToDegrees(&attitude);
		RadiansToDegrees(&bank);
		return;
	}
	double sqx = qx*qx;
	double sqy = qy*qy;
	double sqz = qz*qz;
	heading = (float) atan2((double)2.0*qw*qz-2.0*qx*qy , (double)1 - 2.0*sqz - 2.0*sqx);
	attitude = (float)asin(2.0*test);
	bank = (float) atan2((double)2.0*qw*qy-2.0*qx*qz , (double)1.0 - 2.0*sqy - 2.0*sqx);
	RadiansToDegrees(&heading);
	RadiansToDegrees(&attitude);
	RadiansToDegrees(&bank);
}

*/
