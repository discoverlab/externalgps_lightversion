//mavlink lib
#define MAVLINK_USE_CONVENIENCE_FUNCTIONS
#include "./mavlink/include/autoquad/mavlink.h"

void ExternalGPS::loop(void)
{
    int init_flag = 1;
    uint8_t buf[2014];
	mavlink_message_t msg;
	uint16_t len;
    while(1){
       
		//get position data from GPS resources; now it should be the SLAM thread	
		recvfrom(DataSocket, szData, sizeof(szData), 0, (sockaddr *)&TheirAddress, (socklen_t *)&addr_len);
        Unpack(szData,quadID);				//set x,y,z,yaw in Unpack()


		// package the position data into mavlink format
        if(init_flag == 1){
				//fisrt message should also be in this form to tell the UAV that GPS signal is received
                mavlink_msg_extern_gps_position_pack(1,1,&msg,
						        3, 0.1, 0.1, 0.1 ,0.1f, 1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,10000.0f, 0.0f,0.0f, 0.0f);

              len = mavlink_msg_to_send_buffer(buf, &msg);              
			  //this function should be replaced by bluetooth_sending
			  serial.serialsendf((const char*)buf,len);
              init_flag = 0;
        }
        else {
			 //transmit position data: latitude, longtitude, altitude, yaw angle
             mavlink_msg_extern_gps_position_pack(1,1,&msg,
						        4, Lat_mav, Lon_mav, Alt_mav ,0.0f, 0.0f,Yaw_mav,0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);

            len = mavlink_msg_to_send_buffer(buf, &msg);
			//this function should be replaced by bluetooth_sending
			serial.serialsendf((const char*)buf,len);
        }


    }
}